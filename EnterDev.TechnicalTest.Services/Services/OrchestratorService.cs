﻿namespace EnterDev.TechnicalTest.Services.Services
{
    using EnTerDev.TechnicalTest.Entities.Helpers;
    using EnTerDev.TechnicalTest.Entities.Models;
    using System.Linq;
    using Infraestructure.Integrations;
    using System.Xml.Linq;
    using System.Collections.Generic;

    public class OrchestratorService
    {
        public OrchestratorService()
        {
        }

        /// <summary>
        /// Orquestar funcionalidades del grupo de autos
        /// </summary>
        /// <returns>List GroupModel</returns>
        public List<GroupModel> GetGroupsAutos()
        {
            UrlHelper urlh = new UrlHelper();          
            string xml = ExecuteService();
            return FormatXml(xml);
        }

        /// <summary>
        /// Realiza el llamado a la capa de infraestructura para el consumo del web service GET
        /// </summary>
        /// <returns>string XML</returns>
        private string ExecuteService()
        {
            var urlHelper = new UrlHelper();
            var integration = new IntegrationClass();
            string url = urlHelper.GetUrlsAutos();

            return integration.GetServices(url);
        }

        /// <summary>
        /// Se encarga de realizar una lista con los objetos del XML
        /// </summary>
        /// <param name="xml">cadena de xml</param>
        /// <typeparam>string</typeparam>
        /// <returns>List GroupModel</returns>
        private List<GroupModel> FormatXml(string xml)
        {

            try
            {
                XDocument data = XDocument.Parse(xml);

                List<GroupModel> groupModelList = GetGroupModelList(data);
                foreach (var groupModel in groupModelList)
                {
                    groupModel.autoModelList = GetAutoModelForEachGroup(groupModel, data);
                }

                return groupModelList;
            }
            catch (System.Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Se encarga de realizar el modelamiento al XML en el objeto GroupModel
        /// </summary>
        /// <param name="xml">Objeto XDocument</param>
        /// <typeparam>XDocument</typeparam>
        /// <returns>List GroupModel</returns>
        private List<GroupModel> GetGroupModelList(XDocument xml)
        {
            var groupList = from row in xml.Descendants("group").AsEnumerable()
                            select new GroupModel()
                            {
                                cid = row.Attribute("cid").Value,
                                gid = row.Attribute("gid").Value,
                                name = row.Attribute("name").Value
                            };

            return groupList.ToList();
        }

        /// <summary>
        /// Se encarga de realizar el modelamiento al XML en el objeto AutoModel por cada uno de los grupos
        /// </summary>
        /// <param name="xml">Objeto XDocument</param>
        /// <typeparam>XDocument</typeparam>
        /// <param name="groupModel">Grupo por el cual se agrupan los autos</param>
        /// <typeparam>GroupModel</typeparam>
        /// <returns>List GroupModel</returns>
        private List<AutoModel> GetAutoModelForEachGroup(GroupModel groupModel, XDocument xml)
        {

            var autoList = from auto in xml.Descendants("auto").AsEnumerable()
                           where auto.Parent.Attribute("cid").Value.Equals(groupModel.cid)
                           select new AutoModel()
                           {
                               name = auto.Attribute("name").Value,
                               file = auto.Element("file").Value,
                               auto_id = auto.Element("auto_id").Value,
                               realtime = auto.Element("realtime").Value,
                               hash = auto.Element("hash").Value,
                               hidden = auto.Element("hidden").Value,
                               key = auto.Element("key").Value,
                               url = auto.Element("url").Value
                           };

            return autoList.ToList();
        }

        /// <summary>
        /// Realiza el llamado a la capa de infraestructura para realiza el download de un archivo
        /// </summary>
        /// <param name="fileName"> Nombre del archivo</param>
        /// <typeparam>String</typeparam>
        /// <param name="idAuto"> Id del auto</param>
        /// <typeparam>String</typeparam>
        /// <returns>string URL</returns>
        public string DownloadFile(string fileName, string idAuto) {
            IntegrationClass IntegrationClass = new IntegrationClass();
            UrlHelper urlHelper = new UrlHelper();

            string fullFileName = $"{idAuto}_{fileName}";
            string urlService = urlHelper.GetUrlsEnterDevUrlFiles(fileName);
            string urlServer = urlHelper.GetDownloadPath(fullFileName);
            
            IntegrationClass.GetFile(urlService, urlServer);

            return urlHelper.GetUrlApplication(fullFileName);

        }

    }
}
