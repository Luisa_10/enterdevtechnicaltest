﻿namespace EnterDev.TechnicalTest.Infraestructure.Integrations
{
    using RestSharp;
    using System.IO;
    using System.Net;

    public class IntegrationClass
    {
        public IntegrationClass()
        {
        }

        /// <summary>
        /// Realiza el consumo de un webservice GET
        /// </summary>
        /// <param name="url">Url del servicio</param>
        /// <typeparam>string</typeparam>
        /// <returns> string contenido de la respuesta</returns>
        public string GetServices(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response = client.Execute(request);

            return response.Content;
        }

        /// <summary>
        /// Descarga un archivo de una ruta al equipo local
        /// </summary>
        /// <param name="urlServiceFile"> Url del servicio</param>
        /// <typeparam>string</typeparam>
        /// <param name="urlServerDownload">Ruta donde su guardará el archivo</param>
        /// <typeparam>string</typeparam>
        public void GetFile(string urlServiceFile, string urlServerDownload)
        {
            using (var webClient = new WebClient())
            {
                webClient.DownloadFile(urlServiceFile, urlServerDownload);
                webClient.Dispose();
            }
        }
    }
}
