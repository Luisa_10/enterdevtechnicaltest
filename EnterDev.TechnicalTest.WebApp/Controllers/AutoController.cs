﻿using EnTerDev.TechnicalTest.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnterDev.TechnicalTest.Services.Services;

namespace EnterDev.TechnicalTest.WebApp.Controllers
{
    public class AutoController : Controller
    {
        // GET: Auto
        public ActionResult Index()
        {
            OrchestratorService OrchestratorService = new OrchestratorService();
            List<GroupModel> autos = OrchestratorService.GetGroupsAutos();

            if (TempData["url"] != null)
            {
                ViewBag.Url = TempData["url"];
            }

            return View(autos);
        }

        [HttpGet]
        public ActionResult DownloadFile(string fileName, string idAuto)
        {
            OrchestratorService OrchestratorService = new OrchestratorService();
            string url = OrchestratorService.DownloadFile(fileName, idAuto);
            TempData["url"] = url;

            return RedirectToAction("Index");
        }
    }
}