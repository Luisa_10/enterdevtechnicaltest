﻿namespace EnTerDev.TechnicalTest.Entities.Helpers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;


    public class UrlHelper
    {

        public UrlHelper()
        {
        }
        /// <summary>
        /// Retorna Url web services EnterDev
        /// </summary>
        /// <returns>String Url</returns>
        public string GetUrlsAutos()
        {
            string url = ConfigurationManager.AppSettings["UrlGetAuto"];
            string machine = ConfigurationManager.AppSettings["machine"];
            string domain = ConfigurationManager.AppSettings["domain"];
            string username = ConfigurationManager.AppSettings["username"];
            string format = ConfigurationManager.AppSettings["format"];

            return string.Format(url, machine, domain, username, format);
        }

        /// <summary>
        /// Retorna Url de descarga de archivos EnterDev
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public string GetUrlsEnterDevUrlFiles(string fileName)
        {
            return $"{ConfigurationManager.AppSettings["EnterDevUrlFiles"]}{fileName}";
        }

        /// <summary>
        /// Retorna la ruta de descarga de archivos
        /// </summary>
        /// <param name="fileName"> Nombre del archivo</param>
        /// <returns></returns>
        public string GetDownloadPath(string fileName)
        {
            return $"{ConfigurationManager.AppSettings["DownloadPath"]}{fileName}";
        }

        /// <summary>
        /// Restorna la Url Local de archivo
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public string GetUrlApplication(string fileName)
        {
            return $"{ConfigurationManager.AppSettings["UrlApplication"]}{fileName}";
        }
        

    }
}
