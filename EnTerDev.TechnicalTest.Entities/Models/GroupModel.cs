﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnTerDev.TechnicalTest.Entities.Models
{
    public class GroupModel
    {
        public GroupModel()
        {

        }

        public string cid { get; set; }

        public string gid { get; set; }

        public string name { get; set; }

        public List<AutoModel> autoModelList { get; set; }
    }
}
