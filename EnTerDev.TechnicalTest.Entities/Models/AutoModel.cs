﻿using System.Xml.Serialization;

namespace EnTerDev.TechnicalTest.Entities.Models
{

    public class AutoModel { 
    
        public string name { set; get; }
       
        public string file { set; get; }
 
        public string auto_id { set; get; }
       
        public string realtime { set; get; }
           
        public string hash { set; get; }
       
        public string hidden { set; get; }

        public string key { set; get; }
       
        public string url { set; get; }

        public AutoModel(string name, string file, string auto_id, string realtime, string hash, string hidden, string key, string url)
        {
            this.name = name;
            this.file = file;
            this.auto_id = auto_id;
            this.realtime = realtime;
            this.hash = hash;
            this.hidden = hidden;
            this.key = key;
            this.url = url;
        }

        public AutoModel()
        {
        }
    }
}
